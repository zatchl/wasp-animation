import arcade
import datetime
from openpyxl import load_workbook
from typing import List, Dict

# The total duration of the animation in seconds
ANIMATION_DURATION = 20

# The radius of the circles representing each wasp
WASP_RADIUS = 20

# The size of the window containing the animation
WINDOW_WIDTH = 1500
WINDOW_HEIGHT = 1000

# The size of the grid containing the nests
GRID_WIDTH = WINDOW_WIDTH
GRID_HEIGHT = WINDOW_HEIGHT * 0.95

# The number of rows and columns in the grid
GRID_ROWS = 8
GRID_COLUMNS = 5

# The width of the black line separating each cell in the grid
GRID_LINE_WIDTH = 5

# The size of each cell in the grid
GRID_CELL_WIDTH = ((GRID_WIDTH - GRID_LINE_WIDTH) / GRID_COLUMNS) - GRID_LINE_WIDTH
GRID_CELL_HEIGHT = ((GRID_HEIGHT - GRID_LINE_WIDTH) / GRID_ROWS) - GRID_LINE_WIDTH


class Wasp:
    """ This class manages a wasp and its position on the screen. """

    def __init__(self, name, positions, radius):
        self.name = name
        self.positions = positions
        self.radius = radius
        self.colors = self.colors_from_name()
        self.position_index = 0

    def draw(self):
        """ Draws the wasp at the current position. """

        # If we've reached the end of the animation, set the position index back to zero to start over
        if self.position_index > len(self.positions) - 1:
            self.position_index = 0

        # If the position coordinate at this index is less than 0, the wasp is not in any nest and should not be drawn
        # Make sure to still increment the position index
        if self.positions[self.position_index][0] < 0:
            self.position_index += 1
            return

        # Draw the 4 quadrants of the wasp and the black outline of the wasp
        arcade.draw_arc_filled(self.positions[self.position_index][0], self.positions[self.position_index][1],
                               self.radius, self.radius, self.colors[0], 90, 180)
        arcade.draw_arc_filled(self.positions[self.position_index][0], self.positions[self.position_index][1],
                               self.radius, self.radius, self.colors[1], 0, 90)
        arcade.draw_arc_filled(self.positions[self.position_index][0], self.positions[self.position_index][1],
                               self.radius, self.radius, self.colors[2], 180, 270)
        arcade.draw_arc_filled(self.positions[self.position_index][0], self.positions[self.position_index][1],
                               self.radius, self.radius, self.colors[3], 270, 360)
        arcade.draw_circle_outline(self.positions[self.position_index][0], self.positions[self.position_index][1],
                                   self.radius, arcade.color.BLACK)

        # Increment the position index so that the next time this function is called it will use the next coordinate
        # in self.positions
        self.position_index += 1

    def colors_from_name(self):
        """ Get a list of colors from name of the wasp (RGWB) """
        colors = []
        for c in self.name:
            if c == 'R':
                colors.append(arcade.color.RED)
            elif c == 'G':
                colors.append(arcade.color.GREEN)
            elif c == 'B':
                colors.append(arcade.color.BLUE)
            elif c == 'Y':
                colors.append(arcade.color.YELLOW)
            elif c == 'P':
                colors.append(arcade.color.PURPLE)
            elif c == 'W':
                colors.append(arcade.color.WHITE)
            elif c == 'O':
                colors.append(arcade.color.ORANGE)
        return colors


class WaspAnimation(arcade.Window):
    """
    Main application class.
    """

    def __init__(self, width, height):
        super().__init__(width, height)
        # Store the values for the grid
        self.grid = []
        for row in range(GRID_ROWS):
            self.grid.append([])
            for column in range(GRID_COLUMNS):
                # Do the math to figure out where the cell is
                x = (GRID_LINE_WIDTH + GRID_CELL_WIDTH) * column + GRID_LINE_WIDTH + GRID_CELL_WIDTH // 2
                y = (GRID_LINE_WIDTH + GRID_CELL_HEIGHT) * row + GRID_LINE_WIDTH + GRID_CELL_HEIGHT // 2
                self.grid[row].append((x, y))

        wb = load_workbook('WaspCensus2018.xlsx')
        ws = wb['Census without Nest Data']
        dates = [cell.value for cell in next(ws.rows) if type(cell.value) is datetime.datetime]

        # Get all the wasps in the worksheet
        wasp_dict: Dict[str, List[List[int]]] = dict()
        for row_num, row in enumerate(ws.iter_rows(min_row=1, max_row=41)):
            for col_num, cell in enumerate(row):
                if cell.value is not None:
                    wasps_in_cell = 0
                    cell_string = str(cell.value)
                    strings_in_cell = cell_string.split(',')
                    for name in strings_in_cell:
                        name = name.strip()
                        if name.isupper() and len(name) == 4 and '_' not in name and '?' not in name:
                            if name not in wasp_dict:
                                wasp_dict[name] = [[-1, -1]] * len(dates)
                            wasp_dict[name][col_num - 1] = [row_num, wasps_in_cell]
                            wasps_in_cell += 1

        # Create the list of wasps using the dictionary of wasps
        self.wasps = [Wasp(wasp, self.interpolate_positions(wasp_dict[wasp], ANIMATION_DURATION), WASP_RADIUS) for wasp
                      in wasp_dict]

        # Create the list of dates that correspond with the list of positions that each wasp contains
        self.interpolated_dates = self.interpolate_dates(dates, len(self.wasps[0].positions))
        self.date_index = 0

    @staticmethod
    def interpolate_dates(dates, total_dates):
        interpolated_dates_per_date = total_dates // len(dates)
        interpolated_dates = []

        for date in dates:
            for i in range(interpolated_dates_per_date):
                interpolated_dates.append(date)

        return interpolated_dates

    def interpolate_positions(self, positions, animation_duration):
        """ Convert a list of discrete positions into a list of interpolated positions that will animate smoothly """
        total_positions = 60 * animation_duration
        interpolated_positions_per_position = total_positions // len(positions)
        interpolated_positions = []

        for index, position in enumerate(positions):
            current_nest_position = self.nest_position(position[0], position[1])
            for i in range(interpolated_positions_per_position):
                interpolated_positions.append(current_nest_position)

            if index < len(positions) - 1:
                next_nest_position = self.nest_position(positions[index + 1][0], positions[index + 1][1])
                if next_nest_position[0] < 0 or current_nest_position[0] < 0:
                    for i in range(interpolated_positions_per_position):
                        interpolated_positions.append((-1, -1))
                    continue
                change_x = (next_nest_position[0] - current_nest_position[0]) / interpolated_positions_per_position
                change_y = (next_nest_position[1] - current_nest_position[1]) / interpolated_positions_per_position
                for i in range(interpolated_positions_per_position):
                    interpolated_positions.append((current_nest_position[0] + change_x * i, current_nest_position[1] +
                                                   change_y * i))

        return interpolated_positions

    def nest_position(self, nest_index, wasp_number):
        """ Get the screen coordinates of a nest from its index """
        if nest_index < 1:
            return -1, -1

        grid_row = (nest_index - 1) // GRID_COLUMNS
        grid_column = (nest_index - 1) % GRID_COLUMNS

        x_pos = self.grid[grid_row][grid_column][0]
        y_pos = self.grid[grid_row][grid_column][1]

        # Because there can be more than one wasp on a nest at a time, each wasp needs to be offset from the center
        if wasp_number == 0:
            x_pos -= (GRID_CELL_WIDTH / 4)
            y_pos += (GRID_CELL_HEIGHT / 4)
        elif wasp_number == 1:
            y_pos += (GRID_CELL_HEIGHT / 4)
        elif wasp_number == 2:
            x_pos += (GRID_CELL_WIDTH / 4)
            y_pos += (GRID_CELL_HEIGHT / 4)
        elif wasp_number == 3:
            x_pos -= (GRID_CELL_WIDTH / 4)
            y_pos -= (GRID_CELL_HEIGHT / 4)
        elif wasp_number == 4:
            y_pos -= (GRID_CELL_HEIGHT / 4)
        elif wasp_number == 5:
            x_pos += (GRID_CELL_WIDTH / 4)
            y_pos -= (GRID_CELL_HEIGHT / 4)

        return x_pos, y_pos

    def on_draw(self):
        arcade.start_render()
        # Draw the grid
        for row in range(GRID_ROWS):
            for column in range(GRID_COLUMNS):
                # Figure out what color to draw the box
                if self.grid[row][column] == 1:
                    color = arcade.color.GREEN
                else:
                    color = arcade.color.WHITE

                # Draw the box
                arcade.draw_rectangle_filled(self.grid[row][column][0], self.grid[row][column][1], GRID_CELL_WIDTH,
                                             GRID_CELL_HEIGHT, color)
                # Draw the cell number
                arcade.draw_text(str(column + 1 + (row * GRID_COLUMNS)), self.grid[row][column][0],
                                 self.grid[row][column][1], arcade.color.BLACK, 14, width=int(GRID_CELL_WIDTH),
                                 align="center", anchor_x="center", anchor_y="center")

        # Draw background for date
        arcade.draw_rectangle_filled(WINDOW_WIDTH / 2, GRID_HEIGHT + (WINDOW_HEIGHT - GRID_HEIGHT) / 2, WINDOW_WIDTH,
                                     WINDOW_HEIGHT - GRID_HEIGHT, arcade.color.WHITE)

        # Draw the date
        if self.date_index > (len(self.interpolated_dates) - 1):
            self.date_index = 0

        arcade.draw_text(str(self.interpolated_dates[self.date_index].strftime("%Y-%m-%d")), WINDOW_WIDTH / 2,
                         GRID_HEIGHT + (WINDOW_HEIGHT - GRID_HEIGHT) / 2, arcade.color.BLACK, 18,
                         width=int(WINDOW_WIDTH), align="center", anchor_x="center", anchor_y="center")
        self.date_index += 1

        # Draw each wasp
        for wasp in self.wasps:
            wasp.draw()

    def on_mouse_press(self, x, y, button, key_modifiers):
        pass

    def update(self, delta_time):
        pass


def main():
    WaspAnimation(WINDOW_WIDTH, WINDOW_HEIGHT)
    arcade.run()


if __name__ == "__main__":
    main()
